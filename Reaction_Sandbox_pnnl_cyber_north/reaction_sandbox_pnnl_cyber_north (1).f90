
module Reaction_Sandbox_Cybernorth_class

  use Reaction_Sandbox_Base_class

  use Global_Aux_module
  use Reactive_Transport_Aux_module

  use PFLOTRAN_Constants_module

  implicit none

  private

#include "petsc/finclude/petscsys.h"
  PetscInt, parameter :: C6H12O6_MASS_STORAGE_INDEX = 1
  PetscInt, parameter :: NH4_MASS_STORAGE_INDEX = 2
  PetscInt, parameter :: O2_MASS_STORAGE_INDEX = 3
  PetscInt, parameter :: HCO3_MASS_STORAGE_INDEX = 4
  PetscInt, parameter :: H_MASS_STORAGE_INDEX = 5
  PetscInt, parameter :: BIOMASS_MASS_STORAGE_INDEX = 6
  PetscInt, parameter :: C2H3O2_MASS_STORAGE_INDEX = 7
  PetscInt, parameter :: C6H13NO5_MASS_STORAGE_INDEX = 8
  PetscInt, parameter :: C8H15NO6_MASS_STORAGE_INDEX = 9

  type, public, &
    extends(reaction_sandbox_base_type) :: reaction_sandbox_cybernorth_type
    PetscInt :: c6h12o6_id 
    PetscInt :: nh4_id 
    PetscInt :: o2_id 
    PetscInt :: hco3_id 
    PetscInt :: h_id 
    PetscInt :: biomass_id 
    PetscInt :: c2h3o2_id 
    PetscInt :: c6h13no5_id 
    PetscInt :: c8h15no6_id 
    PetscReal :: mu_max 
    PetscReal :: vh 
    PetscReal :: k_deg 
    PetscReal :: cc 
    PetscReal :: activation_energy 
    PetscReal :: reference_temperature 

    PetscReal :: nrxn
    PetscBool :: store_cumulative_mass
    PetscInt :: offset_auxiliary
  contains
    procedure, public :: ReadInput => CybernorthRead
    procedure, public :: Setup => CybernorthSetup
    procedure, public :: Evaluate => CybernorthReact
    procedure, public :: Destroy => CybernorthDestroy
  end type reaction_sandbox_cybernorth_type

  public :: CybernorthCreate

contains

! ************************************************************************** !

function CybernorthCreate()
#include "petsc/finclude/petscsys.h"
  use petscsys
  implicit none

  class(reaction_sandbox_cybernorth_type), pointer :: CybernorthCreate

  allocate(CybernorthCreate)
  CybernorthCreate%c6h12o6_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%nh4_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%o2_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%hco3_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%h_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%biomass_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%c2h3o2_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%c6h13no5_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%c8h15no6_id = UNINITIALIZED_INTEGER 
  CybernorthCreate%mu_max = UNINITIALIZED_DOUBLE 
  CybernorthCreate%vh = UNINITIALIZED_DOUBLE 
  CybernorthCreate%k_deg = UNINITIALIZED_DOUBLE 
  CybernorthCreate%cc = UNINITIALIZED_DOUBLE 
  CybernorthCreate%activation_energy = UNINITIALIZED_DOUBLE 
  CyberCreate%reference_temperature = 298.15d0 ! 25 C

  CybernorthCreate%nrxn = UNINITIALIZED_INTEGER
  CybernorthCreate%store_cumulative_mass = PETSC_FALSE

  nullify(CybernorthCreate%next)
  print *, 'CybernorthCreat Done'
end function CybernorthCreate

! ************************************************************************** !

! ************************************************************************** !

subroutine CybernorthRead(this,input,option)
#include "petsc/finclude/petscsys.h"
  use petscsys
  use Option_module
  use String_module
  use Input_Aux_module

  implicit none

  class(reaction_sandbox_cybernorth_type) :: this
  type(input_type), pointer :: input
  type(option_type) :: option

  PetscInt :: i
  character(len=MAXWORDLENGTH) :: word, internal_units, units
  character(len=MAXSTRINGLENGTH) :: error_string

  error_string = 'CHEMISTRY,REACTION_SANDBOX,CYBERNORTH'

  do
    call InputReadPflotranString(input,option)
    if (InputError(input)) exit
    if (InputCheckExit(input,option)) exit

    call InputReadWord(input,option,word,PETSC_TRUE)
    call InputErrorMsg(input,option,'keyword',error_string)
    call StringToUpper(word)

    select case(trim(word))

      case('MU_MAX')
        call InputReadDouble(input,option,this%mu_max)
        call InputErrorMsg(input,option,'mu_max',error_string)
        call InputReadAndConvertUnits(input,this%mu_max,'1/sec', &
                                      trim(error_string)//',mu_max',option)
        
      case('VH')
        call InputReadDouble(input,option,this%vh)
        call InputErrorMsg(input,option,'vh',error_string)
        call InputReadAndConvertUnits(input,this%vh,'m^3', &
                                      trim(error_string)//',vh',option)
        
      case('K_DEG')
        call InputReadDouble(input,option,this%k_deg)
        call InputErrorMsg(input,option,'k_deg',error_string)
        call InputReadAndConvertUnits(input,this%k_deg,'1/sec', &
                                      trim(error_string)//',k_deg',option)
        
      case('CC')
        call InputReadDouble(input,option,this%cc)
        call InputErrorMsg(input,option,'cc',error_string)
        call InputReadAndConvertUnits(input,this%cc,'M', &
                                      trim(error_string)//',cc',option)
        
      case('ACTIVATION_ENERGY')
        call InputReadDouble(input,option,this%activation_energy)
        call InputErrorMsg(input,option,'activation_energy',error_string)
        call InputReadAndConvertUnits(input,this%activation_energy,'J/mol', &
                                      trim(error_string)//',activation_energy',option)
        
      case('REFERENCE_TEMPERATURE')
        call InputReadDouble(input,option,this%reference_temperature)
        call InputErrorMsg(input,option,'reference temperature [C]', &
                           error_string)
        this%reference_temperature = this%reference_temperature + 273.15d0
        
      case default
        call InputKeywordUnrecognized(word,error_string,option)
    end select
  enddo
end subroutine CybernorthRead

! ************************************************************************** !

subroutine CybernorthSetup(this,reaction,option)

  use Reaction_Aux_module, only : reaction_type, GetPrimarySpeciesIDFromName
  use Reaction_Immobile_Aux_module, only : GetImmobileSpeciesIDFromName
  use Reaction_Mineral_Aux_module, only : GetKineticMineralIDFromName
  use Option_module

  implicit none

  class(reaction_sandbox_cybernorth_type) :: this
  type(reaction_type) :: reaction
  type(option_type) :: option

  character(len=MAXWORDLENGTH) :: word
  PetscInt :: irxn

  PetscReal, parameter :: per_day_to_per_sec = 1.d0 / 24.d0 / 3600.d0

  word = 'C6H12O6'
  this%c6h12o6_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'NH4+'
  this%nh4_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'O2'
  this%o2_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'HCO3-'
  this%hco3_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'H+'
  this%h_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'BIOMASS'
  this%biomass_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'C2H3O2-'
  this%c2h3o2_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'C6H13NO5'
  this%c6h13no5_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  word = 'C8H15NO6'
  this%c8h15no6_id = &
    GetPrimarySpeciesIDFromName(word,reaction,option)
    
  if (this%store_cumulative_mass) then
    this%offset_auxiliary = reaction%nauxiliary
    reaction%nauxiliary = reaction%nauxiliary + 18
  endif

end subroutine CybernorthSetup

! ************************************************************************** !

subroutine CybernorthAuxiliaryPlotVariables(this,list,reaction,option)

  use Option_module
  use Reaction_Aux_module
  use Output_Aux_module
  use Variables_module, only : REACTION_AUXILIARY

  implicit none

  class(reaction_sandbox_cybernorth_type) :: this
  type(output_variable_list_type), pointer :: list
  type(option_type) :: option
  type(reaction_type) :: reaction

  character(len=MAXWORDLENGTH) :: names(9)
  character(len=MAXWORDLENGTH) :: word
  character(len=MAXWORDLENGTH) :: units
  PetscInt :: indices(9)
  PetscInt :: i

  names(1) = 'C6H12O6'
  names(2) = 'NH4+'
  names(3) = 'O2'
  names(4) = 'HCO3-'
  names(5) = 'H+'
  names(6) = 'BIOMASS'
  names(7) = 'C2H3O2-'
  names(8) = 'C6H13NO5'
  names(9) = 'C8H15NO6'
  indices(1) = C6H12O6_MASS_STORAGE_INDEX
  indices(2) = NH4_MASS_STORAGE_INDEX
  indices(3) = O2_MASS_STORAGE_INDEX
  indices(4) = HCO3_MASS_STORAGE_INDEX
  indices(5) = H_MASS_STORAGE_INDEX
  indices(6) = BIOMASS_MASS_STORAGE_INDEX
  indices(7) = C2H3O2_MASS_STORAGE_INDEX
  indices(8) = C6H13NO5_MASS_STORAGE_INDEX
  indices(9) = C8H15NO6_MASS_STORAGE_INDEX

  if (this%store_cumulative_mass) then
    do i = 1, 9
      word = trim(names(i)) // ' Rate'
      units = 'mol/m^3-sec'
      call OutputVariableAddToList(list,word,OUTPUT_RATE,units, &
                                   REACTION_AUXILIARY, &
                                   this%offset_auxiliary+indices(i))
    enddo
    do i = 1, 9
      word = trim(names(i)) // ' Cum. Mass'
      units = 'mol/m^3'
      call OutputVariableAddToList(list,word,OUTPUT_GENERIC,units, &
                                   REACTION_AUXILIARY, &
                                   this%offset_auxiliary+9+indices(i))
    enddo
  endif

end subroutine CybernorthAuxiliaryPlotVariables

! ************************************************************************** !

subroutine CybernorthReact(this,Residual,Jacobian,compute_derivative, &
                         rt_auxvar,global_auxvar,material_auxvar,reaction, &
                         option)

  use Option_module
  use Reaction_Aux_module
  use Material_Aux_class

  implicit none

  class(reaction_sandbox_cybernorth_type) :: this
  type(option_type) :: option
  type(reaction_type) :: reaction
  ! the following arrays must be declared after reaction
  PetscReal :: Residual(reaction%ncomp)
  PetscReal :: Jacobian(reaction%ncomp,reaction%ncomp)
  type(reactive_transport_auxvar_type) :: rt_auxvar
  type(global_auxvar_type) :: global_auxvar
  class(material_auxvar_type) :: material_auxvar

  PetscInt, parameter :: iphase = 1
  PetscReal :: L_water
  PetscReal :: kg_water

  PetscInt :: i, j, irxn
  PetscReal :: C_c6h12o6,C_nh4,C_o2,C_hco3,C_h,C_biomass,C_c2h3o2,C_c6h13no5,C_c8h15no6
  PetscReal :: r1doc,r1o2,r2doc,r2o2,r3doc,r3o2,r4doc,r4o2
  PetscReal :: r1kin,r2kin,r3kin,r4kin
  PetscReal :: sumkin
  PetscReal :: u1,u2,u3,u4
  PetscReal :: molality_to_molarity
  PetscReal :: temperature_scaling_factor
  PetscReal :: mu_max_scaled
  PetscReal :: k1_scaled,k2_scaled,k3_scaled,k4_scaled,k_deg_scaled
  PetscReal :: volume, rate_scale
  PetscBool :: compute_derivative

  PetscReal :: rate(4)

  volume = material_auxvar%volume
  L_water = material_auxvar%porosity*global_auxvar%sat(iphase)* &
            volume*1.d3 ! m^3 -> L
  kg_water = material_auxvar%porosity*global_auxvar%sat(iphase)* &
             global_auxvar%den_kg(iphase)*volume

  molality_to_molarity = global_auxvar%den_kg(iphase)*1.d-3

  if (reaction%act_coef_update_frequency /= ACT_COEF_FREQUENCY_OFF) then
    option%io_buffer = 'Activity coefficients not currently supported in &
      &CybernorthReact().'
    call printErrMsg(option)
  endif

  temperature_scaling_factor = 1.d0
  if (Initialized(this%activation_energy)) then
    temperature_scaling_factor = &
      exp(this%activation_energy/IDEAL_GAS_CONSTANT* &
          (1.d0/this%reference_temperature-1.d0/(global_auxvar%temp+273.15d0)))
  endif

  ! concentrations are molarities [M]
  C_c6h12o6 = rt_auxvar%pri_molal(this%c6h12o6_id)* &
        rt_auxvar%pri_act_coef(this%c6h12o6_id)*molality_to_molarity
        
  C_nh4 = rt_auxvar%pri_molal(this%nh4_id)* &
        rt_auxvar%pri_act_coef(this%nh4_id)*molality_to_molarity
        
  C_o2 = rt_auxvar%pri_molal(this%o2_id)* &
        rt_auxvar%pri_act_coef(this%o2_id)*molality_to_molarity
        
  C_hco3 = rt_auxvar%pri_molal(this%hco3_id)* &
        rt_auxvar%pri_act_coef(this%hco3_id)*molality_to_molarity
        
  C_h = rt_auxvar%pri_molal(this%h_id)* &
        rt_auxvar%pri_act_coef(this%h_id)*molality_to_molarity
        
  C_biomass = rt_auxvar%immobile(this%biomass_id-reaction%offset_immobile)
        
  C_c2h3o2 = rt_auxvar%pri_molal(this%c2h3o2_id)* &
        rt_auxvar%pri_act_coef(this%c2h3o2_id)*molality_to_molarity
        
  C_c6h13no5 = rt_auxvar%pri_molal(this%c6h13no5_id)* &
        rt_auxvar%pri_act_coef(this%c6h13no5_id)*molality_to_molarity
        
  C_c8h15no6 = rt_auxvar%pri_molal(this%c8h15no6_id)* &
        rt_auxvar%pri_act_coef(this%c8h15no6_id)*molality_to_molarity
        
  mu_max_scaled = this%mu_max * temperature_scaling_factor
  k_deg_scaled = this%k_deg * temperature_scaling_factor

  r1doc = exp(-0.33836/(this%vh * C_c6h12o6))
  r1o2 = exp(-0.9801799999999999/(this%vh * C_o2))
  r1kin = mu_max_scaled * r1doc * r1o2
  r2doc = exp(-1.14305/(this%vh * C_c2h3o2))
  r2o2 = exp(-1.2361/(this%vh * C_o2))
  r2kin = mu_max_scaled * r2doc * r2o2
  r3doc = exp(-0.34794/(this%vh * C_c6h13no5))
  r3o2 = exp(-1.03764/(this%vh * C_o2))
  r3kin = mu_max_scaled * r3doc * r3o2
  r4doc = exp(-0.25795/(this%vh * C_c8h15no6))
  r4o2 = exp(-1.01363/(this%vh * C_o2))
  r4kin = mu_max_scaled * r4doc * r4o2

  sumkin = r1kin + r2kin + r3kin + r4kin

  u1 = 0.d0
  if (r1kin > 0.d0) u1 = r1kin/sumkin
  u2 = 0.d0
  if (r2kin > 0.d0) u2 = r2kin/sumkin
  u3 = 0.d0
  if (r3kin > 0.d0) u3 = r3kin/sumkin
  u4 = 0.d0
  if (r4kin > 0.d0) u4 = r4kin/sumkin

  rate(1) = u1*r1kin*(1-C_biomass/this%cc)
  rate(2) = u2*r2kin*(1-C_biomass/this%cc)
  rate(3) = u3*r3kin*(1-C_biomass/this%cc)
  rate(4) = u4*r4kin*(1-C_biomass/this%cc)

  Residual(this%c6h12o6_id) = Residual(this%c6h12o6_id)  &
                             + 0.33836 * rate(1) * C_biomass * L_water
  Residual(this%nh4_id) = Residual(this%nh4_id)  &
                         + 0.2 * rate(1) * C_biomass * L_water &
                         + 0.2 * rate(2) * C_biomass * L_water &
                         - 0.14793900000000001 * rate(3) * C_biomass * L_water &
                         - 0.057953 * rate(4) * C_biomass * L_water
  Residual(this%o2_id) = Residual(this%o2_id)  &
                        + 0.9801799999999999 * rate(1) * C_biomass * L_water &
                        + 1.2361 * rate(2) * C_biomass * L_water &
                        + 1.03764 * rate(3) * C_biomass * L_water &
                        + 1.01363 * rate(4) * C_biomass * L_water
  Residual(this%hco3_id) = Residual(this%hco3_id)  &
                          - 1.030176 * rate(1) * C_biomass * L_water &
                          - 1.2861040000000001 * rate(2) * C_biomass * L_water &
                          - 1.087636 * rate(3) * C_biomass * L_water &
                          - 1.063626 * rate(4) * C_biomass * L_water
  Residual(this%h_id) = Residual(this%h_id)  &
                       - 1.230176 * rate(1) * C_biomass * L_water &
                       - 0.343052 * rate(2) * C_biomass * L_water &
                       - 0.9396969999999999 * rate(3) * C_biomass * L_water &
                       - 1.005673 * rate(4) * C_biomass * L_water
  Residual(this%biomass_id) = Residual(this%biomass_id)  &
                             - 1 * rate(1) * C_biomass * L_water &
                             - 1 * rate(2) * C_biomass * L_water &
                             - 1 * rate(3) * C_biomass * L_water &
                             - 1 * rate(4) * C_biomass * L_water
  Residual(this%biomass_id) = Residual(this%biomass_id) + k_deg_scaled * C_biomass * L_water 

  Residual(this%c2h3o2_id) = Residual(this%c2h3o2_id)  &
                            + 1.14305 * rate(2) * C_biomass * L_water
  Residual(this%c6h13no5_id) = Residual(this%c6h13no5_id)  &
                              + 0.34794 * rate(3) * C_biomass * L_water
  Residual(this%c8h15no6_id) = Residual(this%c8h15no6_id)  &
                              + 0.25795 * rate(4) * C_biomass * L_water

  if (this%store_cumulative_mass) then
        rate_scale = C_biomass * L_water / volume
            i = this%offset_auxiliary + C6H12O6_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 0.33836 * rate(1) * rate_scale
        i = this%offset_auxiliary + NH4_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 0.2 * rate(1) * rate_scale &
                        + 0.2 * rate(2) * rate_scale &
                        + 0.14793900000000001 * rate(3) * rate_scale &
                        + 0.057953 * rate(4) * rate_scale
        i = this%offset_auxiliary + O2_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 0.9801799999999999 * rate(1) * rate_scale &
                        + 1.2361 * rate(2) * rate_scale &
                        + 1.03764 * rate(3) * rate_scale &
                        + 1.01363 * rate(4) * rate_scale
        i = this%offset_auxiliary + HCO3_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 1.030176 * rate(1) * rate_scale &
                        + 1.2861040000000001 * rate(2) * rate_scale &
                        + 1.087636 * rate(3) * rate_scale &
                        + 1.063626 * rate(4) * rate_scale
        i = this%offset_auxiliary + H_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 1.230176 * rate(1) * rate_scale &
                        + 0.343052 * rate(2) * rate_scale &
                        + 0.9396969999999999 * rate(3) * rate_scale &
                        + 1.005673 * rate(4) * rate_scale
        i = this%offset_auxiliary + BIOMASS_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 1 * rate(1) * rate_scale &
                        + 1 * rate(2) * rate_scale &
                        + 1 * rate(3) * rate_scale &
                        + 1 * rate(4) * rate_scale
        i = this%offset_auxiliary + C2H3O2_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 1.14305 * rate(2) * rate_scale
        i = this%offset_auxiliary + C6H13NO5_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 0.34794 * rate(3) * rate_scale
        i = this%offset_auxiliary + C8H15NO6_MASS_STORAGE_INDEX
        rt_auxvar%auxiliary_data(i) = &
                        + 0.25795 * rate(4) * rate_scale

  endif
    
end subroutine CybernorthReact


! ************************************************************************** !

subroutine CybernorthDestroy(this)
  use Utility_module

  implicit none

  class(reaction_sandbox_cybernorth_type) :: this

  print *, 'CybernorthDestroy Done'

end subroutine CybernorthDestroy

end module Reaction_Sandbox_Cybernorth_class
